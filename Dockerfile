FROM tomcat:latest
EXPOSE 8080
RUN mv /usr/local/tomcat/webapps /usr/local/tomcat/webapps2
RUN mv /usr/local/tomcat/webapps.dist /usr/local/tomcat/webapps
COPY ./webapp.war /usr/local/tomcat/webapps/
CMD sh /usr/local/tomcat/bin/startup.sh
